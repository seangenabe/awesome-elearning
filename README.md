# awesome-elearning

## Tech

* [Katacoda](https://www.katacoda.com/) (free) - Popular courses include Kubernetes, Docker, and machine learning
  * Playgrounds let you test Kubernetes, Docker, and CoreOS.
* [Android Developer Guides](https://developer.android.com/guide/) (free)
* [Microsoft Virtual Academy](https://mva.microsoft.com/) (free)
* [NodeSchool](https://nodeschool.io/) (free) - Learn web and web-related technologies such as HTML, JavaScript, and bash.
* [Google Developers Training](https://developers.google.com/training/) - Training for web, and Android and other mobile devices.
* [Google Codelabs](https://codelabs.developers.google.com/) - Step-by-step guides on various topics

## General purpose 

* [edX](https://www.edx.org/) (free, paid cert, retake, timed optional) - By MIT, Harvard
* [Open Yale Courses](https://oyc.yale.edu/) (Yale University) (free, no registration required, paid cert)
* [Khan Academy](https://www.khanacademy.org/) (free) - Learn via whiteboard-style YouTube videos.
* [MIT OpenCourseWare](https://ocw.mit.edu/index.htm) (MIT) - Access to MIT's teaching materials
* [OpenCourseWare Consortium](http://www.oeconsortium.org/) - College materials organized as courses
* [Udemy](https://www.udemy.com/) (free/paid, free cert/paid cert)
* [Udacity](https://www.udacity.com/)
* [Coursera](https://www.coursera.org/)

## Other lists

* https://github.com/demicon/mooc
* https://github.com/ForrestKnight/open-source-cs - Computer Science courses